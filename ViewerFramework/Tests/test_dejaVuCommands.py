#
# Author: Sowjanya Karnati
#
#
#$Id: test_dejaVuCommands.py,v 1.3 2008/09/11 17:55:11 vareille Exp $
#


import sys
import unittest
from ViewerFramework.VF import ViewerFramework
from ViewerFramework.dejaVuCommands import setbackgroundcolorCommand,setAntialiasingCommand
from DejaVu.IndexedPolylines import IndexedPolylines
def pause(self, sleepTime=0.4):
        from time import sleep
        sleep(sleepTime)

class DejaVuCommandBaseTests(unittest.TestCase):
    def setUp(self):
        from ViewerFramework.VF import ViewerFramework
        self.vf = ViewerFramework(logMode='no', 
                                  trapExceptions=False, withShell=0)
        self.vf.browseCommands('dejaVuCommands',package='ViewerFramework')
        self.vf.GUI.dockCamera()
    
    
        
    def tearDown(self):
        self.vf.Exit(0)


class SetBackGroungColorTest(DejaVuCommandBaseTests):
    
    def test_settingbgcolor_white(self):
        """tests setting bg color to white"""
        color = [1.0,1.0,1.0]
        self.vf.setbackgroundcolor(color)
        self.assertEqual(self.vf.GUI.VIEWER.currentCamera.backgroundColor[:-1],color)
        

    def test_settingbgcolor_black(self):
        """tests setting bg color to black"""
        color = [0.0,0.0,0.0]
        self.vf.setbackgroundcolor(color)
        self.assertEqual(self.vf.GUI.VIEWER.currentCamera.backgroundColor[:-1],color)
        

    def test_settingbgcolor_blue(self):
        """tests setting bg color to blue"""
        color = [0.0,0.0,1.0]
        self.vf.setbackgroundcolor(color)
        self.assertEqual(self.vf.GUI.VIEWER.currentCamera.backgroundColor[:-1],color)
        

    def test_settingbgcolor_green(self):
        """tests setting bg color to green"""
        color = [0.0,1.0,0.0]
        self.vf.setbackgroundcolor(color)
        self.assertEqual(self.vf.GUI.VIEWER.currentCamera.backgroundColor[:-1],color)
        
    
    def test_settingbgcolor_red(self):
        """tests setting bg color to red"""
        color = [1.0,0.0,0.0]
        self.vf.setbackgroundcolor(color)
        self.assertEqual(self.vf.GUI.VIEWER.currentCamera.backgroundColor[:-1],color)
        
        
    def test_settingbgcolor_invalid_input_1(self):
         """tests command with invalid input"""
         color = []
         c = self.vf.setbackgroundcolor(color)
         error = "Error: color length should be 3"
         self.assertEqual(c,error)
    
    def test_settingbgcolor_invalid_input_2(self): 
         """tests command with invalid input with len(list)>3"""
         color = [1.0,0.2,0.2,0.2]
         c = self.vf.setbackgroundcolor(color)
         error1 = "Error: color length should be 3"
         self.assertEqual(c,error1)
    
    def test_settingbgcolor_invalid_input_3(self):  
         """tests command with invalid input with string
"""
         color = "ftfr"
         c = self.vf.setbackgroundcolor(color)
         error2 = "Error: color type can't be string"
         self.assertEqual(c,error2)    


    ################widget tests###################
    def test_settingbgcolor_widget(self):
        """tests selecting color in widget"""
        color = [1.0,1.0,0.0]
        c = self.vf.setbackgroundcolor
        form = c.showForm('setbackgroundcolor', modal=0, blocking=0)
        c.color_cb(color)
        self.assertEqual(self.vf.GUI.VIEWER.currentCamera.backgroundColor[:-1],color)
        form.withdraw()


class setAntiAliasngTest(DejaVuCommandBaseTests):


    def test_setantialiasing_1(self):
        """tests setting antialiasing val to 3"""
        aa = 3
        triangle=IndexedPolylines('mytriangle',vertices=[[-15,0,0],[0,-10,0],[15,10,0]],
                                   faces=((0,1),(1,2),(2,0),))
        self.vf.GUI.VIEWER.AddObject(triangle)
        c = self.vf.setAntialiasing(aa)
        self.assertEqual(self.vf.GUI.VIEWER.GUI.nbJitter.get(),aa)


    def test_setantialiasing_2(self):
        """tests setting antialiasing val to 15"""
        aa = 15
        triangle=IndexedPolylines('mytriangle',vertices=[[-15,0,0],[0,-10,0],[15,10,0]],
                                   faces=((0,1),(1,2),(2,0),))
        self.vf.GUI.VIEWER.AddObject(triangle)
        c = self.vf.setAntialiasing(aa)
        self.assertEqual(self.vf.GUI.VIEWER.GUI.nbJitter.get(),aa)


    def test_setantialiasing_3(self):
        """tests setting antialiasing val to 66"""
        aa = 66
        triangle=IndexedPolylines('mytriangle',vertices=[[-15,0,0],[0,-10,0],[15,10,0]],
                                   faces=((0,1),(1,2),(2,0),))
        self.vf.GUI.VIEWER.AddObject(triangle) 
        c = self.vf.setAntialiasing(aa)
        self.assertEqual(self.vf.GUI.VIEWER.GUI.nbJitter.get(),aa)


    def test_setantialiasing_invalid_input(self):
        """tests command with invalid input"""
        aa =1
        triangle=IndexedPolylines('mytriangle',vertices=[[-15,0,0],[0,-10,0],[15,10,0]],
                                   faces=((0,1),(1,2),(2,0),))
        self.vf.GUI.VIEWER.AddObject(triangle) 
        c = self.vf.setAntialiasing
        self.assertRaises(ValueError,c,aa)


###############widget tests###############3


    def test_settingantialiasing_widget(self):
        """tests by selecting antialiasing val in widget"""
        aa = 2
        c = self.vf.setAntialiasing
        form = c.showForm('setAntialiasing', modal=0, blocking=0)
        cf = c.cmdForms['setAntialiasing']
        ebn = cf.descr.entryByName
        cmdW = ebn['value']['widget']
        cmdW.select_clear(0,last=1)
        cmdW.select_set(1)
        val = cmdW.getcurselection()[0]
        self.assertEqual(val,aa)
        form.withdraw()

if __name__ == '__main__':
    unittest.main()









         
