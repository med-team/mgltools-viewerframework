#$Header: /opt/cvs/python/packages/share1.5/ViewerFramework/Tests/test_grid3DCommands.py,v 1.3 2010/05/26 23:27:59 annao Exp $
#
#$Id: test_grid3DCommands.py,v 1.3 2010/05/26 23:27:59 annao Exp $
import unittest, sys, os, time

from ViewerFramework.VF import ViewerFramework
ct = 0
totalCt = 8
vf = None

class Grid3DTest(unittest.TestCase):
    """Base class for grid3DCommands unittest"""

    def setUp(self):
        global vf
        if not vf:
            #print "creating vf"
            vf = self.vf = ViewerFramework(withShell=0)
        else:
            self.vf = vf
            
    def tearDown(self):
        global ct
        ct = ct + 1
        if ct == totalCt: self.vf.Exit(0)    
               
    def test_Grid3DCommands_1show_hide(self):
        """Tests if we can call show/hide methods of Grid3DCommands"""
        self.vf.Grid3DCommands.show()
        self.vf.Grid3DCommands.hide()
        

    def test_Grid3DCommands_2read(self):
        """Tests read method"""
        self.vf.Grid3DReadAny(os.path.join('Data', 'small.C.map'))

    def test_Grid3DCommands_3select(self):
        """Tests select methods"""
        self.vf.Grid3DCommands.mlb.selection_set('end')
        
    def test_Grid3DCommands_4_Isocontour(self):
        """Tests add Isocontour"""
        self.x = 10 #addBar expects event with x attribute
        self.vf.Grid3DCommands.Isocontour()
        self.vf.Grid3DIsocontour.ifd[0]['widget'].addBar(self)

    def test_Grid3DCommands_5_OrthoSlice(self):
        """Tests OrthoSlice"""
        self.vf.Grid3DCommands.OrthoSlice()

    def test_Grid3DCommands_6_VolRender(self):
        """Tests VolRender"""
        if self.vf.Grid3DCommands.Checkbuttons.has_key('VolRen'):
            self.vf.Grid3DCommands.VolRen()
        else:
            print "Volume Renderer is not tested "

    def test_Grid3DCommands_7_Isocontour(self):
        """Tests back to Isocontour"""
        self.vf.Grid3DCommands.Isocontour()
        
    def test_Grid3DCommands_8_remove(self):
        """Tests remove method"""
        self.vf.Grid3DAddRemove.remove()

if __name__ == '__main__':
        unittest.main()

    
