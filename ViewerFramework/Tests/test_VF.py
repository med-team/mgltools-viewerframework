"""
This module implements a set of functions to test the changeFont command
"""
import sys
import unittest
## class VFConstructorTest(unittest.TestCase):
##     def test_VFConstructor1(self):
##         from ViewerFramework.VF import ViewerFramework
##         self.vf = ViewerFramework(gui=0)
##         self.failUnless(self.vf.hasGui==0, msg="ViewerFramework created with gui=0 expecting self.vf.hasGui to be equal to 0")
##         self.vf.Exit(0)

##     def test_VFContructor2(self):
##         from ViewerFramework.VF import ViewerFramework
##         self.vf = ViewerFramework(withShell=False)
##         self.AssertEqual(self.vf.GUI.pyshell, None, msg="pyshell should not have been created")
##         self.vf.Exit(0)

##     def test_VFConstructor_withShell(self):
##         from ViewerFramework.VF import ViewerFramework
##         self.vf = ViewerFramework(withShell=True)
##         from idlelib.PyShell import PyShell
##         self.failUnless(not isinstance(self.vf.GUI.pyshell, PyShell), msg="Expectin self.GUI.pyshell to be an instance of idlelib.PyShell.PyShell")
##         self.vf.Exit(0)

from ViewerFramework.VF import ViewerFramework

class VFBasicTests(unittest.TestCase):
    def setUp(self):
        from ViewerFramework.VF import ViewerFramework
        self.vf = ViewerFramework(logMode='no', 
                                  trapExceptions=False, withShell=0)
        
    def pause(self, sleepTime=0.4):
        from time import sleep
        sleep(sleepTime)

    def tearDown(self):
        self.vf.Exit(0)

class VFConstructorTests(unittest.TestCase):

    def test_trapExceptions(self):
        """
        check that if vf.trapExceptions is true:
        the python shell opens when an exception occurs 
        """
        vf = ViewerFramework(logMode='no')
        vf.browseCommands('dejaVuCommands')
        vf.setCameraSize('a','b')
        self.assertEqual(vf.guiVisible,1)
        vf.Exit(0)


    def test_doNotTrapExceptions(self):
        """
        check that if vf.trapExceptions is False:
        the exception is raised
        """
        vf = ViewerFramework(logMode='no', trapExceptions=False)
        vf.browseCommands('dejaVuCommands')
        self.assertRaises(TypeError, vf.setCameraSize,('a','b'))
        vf.Exit(0)


##     def test_twoInstances(self):
##         """
##         check that we can create two instances of ViewerFramework
##         """
##         vf1 = ViewerFramework(withShell=0, logMode='no')
##         vf2 = ViewerFramework(withShell=0, logMode='no')
##         vf1.Exit(0)
##         vf2.Exit(0)

        
        

class VFCameraTests(VFBasicTests):
    def test_dockCamera(self):
        self.vf.GUI.dockCamera()
        dCanvas = self.vf.GUI.vwrCanvasDocked
        dCanvas.update_idletasks()
        ismapped = dCanvas.winfo_ismapped()
        self.assertEqual(ismapped, 1,msg="Expecting 1 instead of %s"%(ismapped))
        self.assertEqual(self.vf.GUI.floatCamVariable.get(), 0, msg="Expecting 0 button is OFF")

    def test_CameraLog(self):
        self.vf.logMode = True
        self.vf.GUI.dockCamera()
        self.vf.GUI.vwrCanvasDocked.update_idletasks()
        
        self.vf.GUI.floatCamera()
                
        dCanvas = self.vf.GUI.vwrCanvasDocked
        dCanvas.update_idletasks()
        self.vf.GUI.dockCamera()
        self.vf.GUI.vwrCanvasDocked.update_idletasks()
        
        txt = self.vf.GUI.MESSAGE_BOX.tx.get(0.0,'end')
        self.assertEqual(txt.split()[-1],'self.GUI.dockCamera(stereo="none")')
        self.assertEqual(txt.split()[-2],'self.GUI.floatCamera(stereo="none")')
    
    
    def test_floatCamera(self):
        import types
        vi = self.vf.GUI.VIEWER.currentCamera
        self.assertEqual(type(vi.getState()['width']), types.IntType)
        self.vf.GUI.dockCamera()
        self.vf.GUI.vwrCanvasDocked.update_idletasks()
        
        self.vf.GUI.floatCamera()
        fCanvas = self.vf.GUI.vwrCanvasFloating
        fCanvas.update_idletasks()
        ismapped = fCanvas.winfo_ismapped()
        self.assertEqual(ismapped, 1, msg="Expecting 1 instead of %s"%ismapped)
        self.assertEqual(self.vf.GUI.floatCamVariable.get(), 1,
                         msg="Expecting 1 button is ON")
       
    def test_floatCamButton(self):
        oldstate = self.vf.GUI.floatCamVariable.get()
        dCanvas = self.vf.GUI.vwrCanvasDocked
        dCanvasState = dCanvas.winfo_ismapped()
        self.vf.GUI.toolbarCheckbuttons['Float_Camera']['Checkbutton'].invoke()
        newstate = self.vf.GUI.floatCamVariable.get()
        dCanvas.update_idletasks()
        dCanvasNewState = dCanvas.winfo_ismapped()
        self.assertEqual(oldstate, not newstate)
        self.assertEqual(dCanvasNewState, not dCanvasState)

    def test_StateRestored(self):
        camState = self.vf.GUI.VIEWER.currentCamera.getState()
        fogState = self.vf.GUI.VIEWER.currentCamera.fog.getState()
        self.vf.GUI.dockCamera()
        newCamState = self.vf.GUI.VIEWER.currentCamera.getState()
        newFogState = self.vf.GUI.VIEWER.currentCamera.getState()
        for k, v in camState.items():
            #if v!=newCamState[k]: 
            #    print "###################"
            #    print 'not equal', k
            #    print "###################"
            #of course width will be different!
            #and the position on the screen changes!!
            if k in ['width', 'rooty']:
                continue
            self.assertEqual(v, newCamState[k])
        for k, v in fogState.items():
            if not newFogState.has_key(k):
                continue
            self.assertEqual(v, newFogState[k])
        
        
if __name__ == '__main__':
    unittest.main()
