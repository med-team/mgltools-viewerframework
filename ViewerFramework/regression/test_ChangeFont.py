#
# $Header: /opt/cvs/python/packages/share1.5/ViewerFramework/regression/test_ChangeFont.py,v 1.2 2003/08/29 18:17:49 sophiec Exp $
#
# $Id: test_ChangeFont.py,v 1.2 2003/08/29 18:17:49 sophiec Exp $
#

"""
This module implements a set of functions to test the changeFont command
"""
import sys
from mglutil.regression import testplus
            
vf = None
# The mglutil.gui.InputForm.Tk.gui.regression.test_InputForm.py test needs
# to be run prior to this suite of tests.

def setup():
    from ViewerFramework.VF import ViewerFramework
    global vf
    vf = ViewerFramework()
    vf.setUserPreference(('trapExceptions', '0'), topCommand = 0)



def quit():
    vf.Exit(0)


def test_changeFont1():
    """
    Function to test changeFont command
    """
    vf.changeFont(('charter',12,'normal'), log = 1)
    assert vf.GUI.ROOT.option_get('font','*')=='charter 12 normal'
    

def test_changeFont2():
    """
    Function to test changeFont command doitWrapper
    NB: 7/8: this may fail because lastCmdLog isn't a string anymore...
    """
    vf.changeFont.doitWrapper(('charter',12,'normal'), log = 1)
    assert vf.GUI.ROOT.option_get('font','*')=='charter 12 normal'
    


harness = testplus.TestHarness( __name__,
                                connect = setup,
                                funs = testplus.testcollect( globals()),
                                disconnect = quit
                                )

if __name__ == '__main__':
    print harness
    sys.exit( len( harness))
