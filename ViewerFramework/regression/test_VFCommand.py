#
# $Header: /opt/cvs/python/packages/share1.5/ViewerFramework/regression/test_VFCommand.py,v 1.5.10.1 2016/03/02 23:04:33 annao Exp $
#
# $Id: test_VFCommand.py,v 1.5.10.1 2016/03/02 23:04:33 annao Exp $
#

"""
This module implements a set of functions to test the VFCommand class
"""
import sys
from mglutil.regression import testplus
from mglutil.gui.InputForm.Tk.gui import InputFormDescr, InputForm
from mglutil.gui.BasicWidgets.Tk.customizedWidgets import ListChooser

import Tkinter, Pmw
vf = None
descr = None
            
# The mglutil.gui.InputForm.Tk.gui.regression.test_InputForm.py test needs
# to be run prior to this suite of tests.

def setup():
    from ViewerFramework.VF import ViewerFramework
    global vf
    vf = ViewerFramework()

def createFormDescr():
    global descr
    descr = InputFormDescr(title = 'My Form')
    descr.append({'name':'checkbutton',
                'widgetType':Tkinter.Checkbutton,
                'wcfg':{'text':'Checkbutton',
                        'variable':Tkinter.IntVar()},
                'gridcfg':{'sticky':'w'}})

    descr.append({'name':'radioselect',
                'widgetType':Pmw.RadioSelect,
                'listtext':['rb1', 'rb2', 'rb3','rb4', 'rb5', 'rb6',
                            'rb7', 'rb8', 'rb9','rb10', 'rb11', 'rb12'],
                'wcfg':{'labelpos':'n',
                        'label_text':'Radiobuttons: ',
                        'orient':'vertical',
                        'buttontype':'radiobutton'},
                'gridcfg':{'sticky':'w'} })
    descr.append({'name':'radioselect2',
                'widgetType':Pmw.RadioSelect,
                'listtext':['rb1', 'rb2', 'rb3','rb4', 'rb5', 'rb6',
                            'rb7', 'rb8', 'rb9','rb10', 'rb11', 'rb12'],
                'wcfg':{'labelpos':'n',
                        'label_text':'Radiobuttons: ',
                        'orient':'horizontal',
                        'buttontype':'radiobutton'},
                'gridcfg':{'sticky':'w'} })
    entries = [('Chocolate',None), ('Vanilla', None), ('Strawberry', None),
               ('Coffee',None), ('Pistachio', None), ('Rasberry',None),
               ]
    descr.append({'name':'listchooser',
                  'widgetType':ListChooser,
                  'wcfg':{'entries':entries},
                  'gridcfg':{'sticky':'w'}
                  })
def quit():
    vf.Exit(0)

###################################################################
## TESTING LOG STRING....   
###################################################################
def test_LOGS():
    """
    """
    vf.source("test2.py")
    logfile = open("mvAll.log.py")


###################################################################
## TESTING COMMANDS USING BUILDFORMDDESCR AND SHOWFORM    
###################################################################
def test_CommandShowForm_1():
    """
    Function to test creation of an inputForm in a command by the
    guiCallback
    showForm is called with default arguments not specified
    """
    createFormDescr()
    ##############################################################
    from ViewerFramework.VFCommand import Command
    class MyCmd(Command):
        def doit(self):
            pass
        def __call__(self):
            self.doitWrapper()
            
        def buildFormDescr(self, formName='myGUI'):
            if formName == 'myGUI':
                idf = descr
                return idf

        def guiCallback(self):
            val = self.showForm('myGUI')
    ##########################################################
    myCmd = MyCmd()
    vf.addCommand(myCmd, 'myCmd', None)
    vf.myCmd.guiCallback()
    assert isinstance(vf.myCmd.cmdForms['myGUI'], InputForm)
    

def test_CommandShowForm_2():
    """
    Function to test creation of an inputForm in a command by the
    guiCallback
    specifying the sFrameCfg argument
    """
    createFormDescr()
    ##############################################################
    from ViewerFramework.VFCommand import Command
    class MyCmd(Command):
        def doit(self):
            pass
        def __call__(self):
            self.doitWrapper()
            
        def buildFormDescr(self, formName='myGUI'):
            if formName == 'myGUI':
                idf = descr
                return idf

        def guiCallback(self):
            val = self.showForm('myGUI', sFrameCfg={'hull_width':500,
                                                     'hull_height':600,
                                                     'hull_bg':'Red'})
    ##########################################################
    myCmd = MyCmd()
    vf.addCommand(myCmd, 'myCmd2', None)
    vf.myCmd2.guiCallback()
    assert isinstance(vf.myCmd2.cmdForms['myGUI'], InputForm)

def test_CommandShowForm_3():
    """
    Function to test creation of an inputForm in a command by the
    guiCallback
    testing the okCfg and cancelCfg arguments
    """
    createFormDescr()
    ##############################################################
    from ViewerFramework.VFCommand import Command
    class MyCmd(Command):
        def doit(self):
            pass
        def __call__(self):
            self.doitWrapper()
            
        def buildFormDescr(self, formName='myGUI'):
            if formName == 'myGUI':
                idf = descr
                return idf
        def validate_cb(self):
            self.action = 'validating'
            self.value = 1

        def dismiss_cb(self):
            self.action = 'dismissing'
            self.value = 0
            
        def guiCallback(self):
            val = self.showForm('myGUI', 
                                okCfg={'text':'Validate',
                                       'command':self.validate_cb},
                                cancelCfg={'text':'Dismiss',
                                           'command':self.dismiss_cb})
            if self.action == 'validating':
                assert self.value
            elif self.action == 'dismissing':
                assert not self.value
    #################################################################
    myCmd = MyCmd()
    vf.addCommand(myCmd, 'myCmd3', None)
    vf.myCmd3.guiCallback()
    assert isinstance(vf.myCmd3.cmdForms['myGUI'], InputForm)


def test_CommandShowForm_4():
    """
    Function to test creation of an inputForm in a command by the
    guiCallback
    Testing the initFunc argument
    """
    createFormDescr()
    ##############################################################
    from ViewerFramework.VFCommand import Command
    class MyCmd(Command):
        def doit(self):
            pass
        def __call__(self):
            self.doitWrapper()
            
        def buildFormDescr(self, formName='myGUI'):
            if formName == 'myGUI':
                idf = descr
                return idf

        def initialize(self, form):
            w = form.descr.entryByName['listchooser']['widget']
            w.add(('Coconut', None))


        def guiCallback(self):
            val = self.showForm('myGUI', initFunc=self.initialize)
            form = self.cmdForms['myGUI']
            ent = form.descr.entryByName['listchooser']['widget'].entries
            assert 'Coconut' in map(lambda x: x[0], ent)
            

    #################################################################
    myCmd = MyCmd()
    vf.addCommand(myCmd, 'myCmd4', None)
    vf.myCmd4.guiCallback()
    assert isinstance(vf.myCmd4.cmdForms['myGUI'], InputForm)



###################################################################
## TESTING COMMANDS USING GETUSERINPUT    
###################################################################
def test_CommandGetUserInput_1():
    """
    Function to test creation of an inputForm in a command by the
    guiCallback
    getUserInput is called with default arguments not specified
    """
    createFormDescr()
    ##############################################################
    from ViewerFramework.VFCommand import Command
    class MyCmd(Command):
        def doit(self):
            pass
        def __call__(self):
            self.doitWrapper()
            
        def guiCallback(self):
            idf = descr
            val = self.vf.getUserInput(idf)
            
    ##########################################################
    myCmd = MyCmd()
    vf.addCommand(myCmd, 'myCmd', None)
    vf.myCmd.guiCallback()
    

def test_CommandGetUserInput_2():
    """
    Function to test creation of an inputForm in a command by the
    guiCallback
    specifying the sFrameCfg argument
    """
    createFormDescr()
    ##############################################################
    from ViewerFramework.VFCommand import Command
    class MyCmd(Command):
        def doit(self):
            pass
        def __call__(self):
            self.doitWrapper()
            
        def buildFormDescr(self, formName='myGUI'):
            if formName == 'myGUI':
                idf = descr
                return idf

        def guiCallback(self):
            idf = descr
            val = self.vf.getUserInput(idf,
                                       sFrameCfg={'hull_width':500,
                                                  'hull_height':600,
                                                  'hull_bg':'Red'})
    ##########################################################
    myCmd = MyCmd()
    vf.addCommand(myCmd, 'myCmd2', None)
    vf.myCmd2.guiCallback()

def test_CommandGetUserInput_3():
    """
    Function to test creation of an inputForm in a command by the
    guiCallback
    testing the okCfg and cancelCfg arguments
    """
    createFormDescr()
    ##############################################################
    from ViewerFramework.VFCommand import Command
    class MyCmd(Command):
        def doit(self):
            pass
        def __call__(self):
            self.doitWrapper()
            
        def validate_cb(self):
            self.action = 'validating'
            self.value = 1

        def dismiss_cb(self):
            self.action = 'dismissing'
            self.value = 0
            
        def guiCallback(self):
            idf = descr
            val = self.vf.getUserInput(idf,
                                       okCfg={'text':'Validate',
                                              'command':self.validate_cb},
                                       cancelCfg={'text':'Dismiss',
                                                  'command':self.dismiss_cb})
            if self.action == 'validating':
                assert self.value
            elif self.action == 'dismissing':
                assert not self.value
    #################################################################
    myCmd = MyCmd()
    vf.addCommand(myCmd, 'myCmd3', None)
    vf.myCmd3.guiCallback()


def test_CommandGetUserInput_4():
    """
    Function to test creation of an inputForm in a command by the
    guiCallback
    Testing the initFunc argument
    """
    createFormDescr()
    ##############################################################
    from ViewerFramework.VFCommand import Command
    class MyCmd(Command):
        def doit(self):
            pass
        def __call__(self):
            self.doitWrapper()
            
        def initialize(self, form):
            w = form.descr.entryByName['listchooser']['widget']
            w.add(('Coconut', None))


        def guiCallback(self):
            idf = descr
            val = self.vf.getUserInput(idf, initFunc=self.initialize)
            form = self.cmdForms['myGUI']
            ent = form.descr.entryByName['listchooser']['widget'].entries
            assert 'Coconut' in map(lambda x: x[0], ent)
            

    #################################################################
    myCmd = MyCmd()
    vf.addCommand(myCmd, 'myCmd4', None)
    vf.myCmd4.guiCallback()


harness = testplus.TestHarness( __name__,
                                connect = setup,
                                funs = testplus.testcollect( globals()),
                                disconnect = quit
                                )

if __name__ == '__main__':
    print harness
    sys.exit( len( harness))
